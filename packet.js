var fs = require('fs'),
	path = require('path');
/*

Reference: fsc-0048.002	fts-0001.016


*/
Function.prototype.bind = function(scope) {
  var _function = this;
  
  return function() {
    return _function.apply(scope, arguments);
  }
}

exports.Packet  = function(raw) {
	var msgs    = [],
		headers = {},
		bundleOffset = 0,
		bundleMessages = [];

	supportsV2plus = function() {
		return checkType(2);
	}

	function parseStoneAge () {
		console.error("FSC-0048 Parse Stone-Age not implemented");
	}

	function checkType(type) {
		if ((headers.CW & Math.pow(2,type-2)) != 0 ) {
			return true
		}
		return false;
	}

	function checkCw() {
		var type;
		
		if (raw.readInt16BE(40) == raw.readInt16LE(44)) { // CWcopies match
			headers.CW = raw.readInt16LE(44);
			return true;
		}
		console.error("checkCw error");
		return false;
	}

	function parsePacket () {
		if (raw.readInt16LE(18) == 2) {
			if (checkCw()) { // CWcopies match
				if ((headers.CW !== 0) && checkType(2)) {
					parseV2Packet();
					if (raw.readInt16LE(50) !== 0 && raw.readInt16LE(20) !== -1) {
						headers.origNet = headers.AuxNet;
					}
					parseBundle(raw.slice(58));
				} else {
					parseStoneAge();
				}
			} else {
				console.error("Stone Age Message")
				parseStoneAge();
			}
		} else {
			console.error("FSC-0048 Process Type-Other not implemented");
		}
	}

	function parseFidoMsg( /* String */ msg ) {
		var area = parseArea(msg),
			result = {};
		if (area != false) {
			result.area = area.area;
			msg = area.msg;
		}

		kludge = parseKludge(msg);
		result.kludge = kludge.kludge;
		result.text   = kludge.msg;
		return result;
	}

	function parseTrailer(msg) {
	}

	function parseArea(msg) {
		var firstLine = msg.substr(0,msg.indexOf('\r')),
			re = /AREA:([^\r]+)/;
		if (re.test(firstLine)) {
			return {
				area: firstLine.substr(5,firstLine.length),
				msg: msg.substr(firstLine.length+1)
			}
		}
		return false;
	}

	function expunge(array, text) { /* utility */
		for (i in array) {
			text = text.replace(new RegExp(array[i]),'')
		}
		return text;
	}

	function parseKludge (msg) {
		var re = new RegExp("\u0001([^:]+):([^\r]+)\r", "g"),
			kludge = {},
			array, xp = [], text=msg;


		// General Kludges
		while ((array = re.exec(msg)) != null) {
			kludge[array[1]] = array[2];
			xp.push(array[0]);
		}

		// Origin
		re=/\r\W*\WOrigin:\W([^\r]+)/
		if ((array = msg.match(re)) != null) {
			kludge.origin = array[1];
			xp.push(re);
		}


		// SEEN-BY
		re = /\rSEEN-BY: ([^\r]+)/g
		kludge['SEEN-BY'] = [];
		while ((array = re.exec(msg)) != null) {
			kludge['SEEN-BY'].push(array[1]);
			xp.push(array[0]);
		}

		text = expunge(xp, text);

		return {
			kludge: kludge,
			msg: text
		}
	}

	function parseBundle (fts0001) {
		if (fts0001.readInt16LE(0) != 2) {
			return;
		}
		var boundary = [],
			message,
			bundle = {
			headers: {
				origNode: fts0001.readInt16LE(2),
				destNode: fts0001.readInt16LE(4),
				origNet: fts0001.readInt16LE(6),
				destNet: fts0001.readInt16LE(8),
				attribute: fts0001.readInt16LE(10),
				cost: fts0001.readInt16LE(12),
				dateTime: fts0001.toString('utf8',14,33)
			}
		};

		bundle.msg = fts0001.slice(34);
		boundary[0] = bundle.msg.slice(0, 36).toString().indexOf('\0'); // toUsername
		boundary[1] = bundle.msg.slice(0, 72).toString().indexOf('\0', boundary[0]+1); // fromUsername
		boundary[2] = bundle.msg.slice(0, 144).toString().indexOf('\0', boundary[1]+1); // subject
		boundary[3] = bundle.msg.toString().indexOf('\0', boundary[2]+1); // text

		try {
			parseBundle(bundle.msg.slice(boundary[3]+1));
		} catch (nextException) {
			console.error("parseBundle nextException")
		}
	
		
		bundle.headers.toUsername = bundle.msg.toString('utf8',0,boundary[0]);
		bundle.headers.fromUsername = bundle.msg.toString('utf8', boundary[0]+1, boundary[1]);
		bundle.headers.subject = bundle.msg.toString('utf8', boundary[1]+1, boundary[2]);
		message = parseFidoMsg(bundle.msg.toString('utf8', boundary[2]+1, boundary[3]));
		message.headers = bundle.headers;
		bundleMessages.push(message);

	}

	function parseV2Packet () {
		var type2      = raw.slice(0,57),
			workBuffer = new Buffer(6);

		workBuffer[0] = type2[41]; workBuffer[1] = type2[40]; // CWvalidationCopy
		workBuffer[2] = type2[24]; workBuffer[3] = type2[42]; // Product Code
		workBuffer[4] = type2[43]; workBuffer[5] = type2[25]; // Product Version
		this.productCode = workBuffer.readInt16LE(2);
		this.productVersion = workBuffer.readInt16LE(4);

		headers.year = type2.readInt16LE(4);
		headers.month = type2.readInt16LE(6);
		headers.day = type2.readInt16LE(8);
		headers.hour = type2.readInt16LE(10);
		headers.min = type2.readInt16LE(12);
		headers.sec = type2.readInt16LE(14);
		headers.baud = type2.readInt16LE(16);
		headers.pktver = type2.readInt16LE(18);
		headers.OrgNet = type2.readInt16LE(20);
		headers.DstNet = type2.readInt16LE(22);
		headers.PrdCodL = type2[24];
		headers.PVMajor = type2[25];
		headers.QOrgZone = type2.readInt16LE(34);
		headers.QDstZone = type2.readInt16LE(36);
		headers.AuxNet = type2.readInt16LE(38);
		headers.CWvalidationCopy = workBuffer.readInt16LE(0);
		headers.PrdCodH = type2[42];
		headers.PVMinor = type2[43];
		headers.CapabilWord = type2.readInt16LE(44);
		headers.OrigZone = type2.readInt16LE(46);
		headers.DestZone = type2.readInt16LE(48);
		headers.OrigPoint = type2.readInt16LE(50);
		headers.DestPoint = type2.readInt16LE(52);
		headers.ProdData = type2.readInt16LE(54);

	}
	parsePacket(this);
	this.headers = headers;
	this.messages = bundleMessages;
}
exports.Packet.fromFile = function(file) {
	try {
		raw = fs.readFileSync(file);
		return new exports.Packet(raw);
	} catch (readFileException) {
		console.error(readFileException);
		throw readFileException;
	}
}